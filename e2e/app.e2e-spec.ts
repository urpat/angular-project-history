import { AngularProjectHistoryPage } from './app.po';

describe('angular-project-history App', function() {
  let page: AngularProjectHistoryPage;

  beforeEach(() => {
    page = new AngularProjectHistoryPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
