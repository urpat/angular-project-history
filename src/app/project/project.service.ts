import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs";

import { Project } from "./project.interface";

@Injectable()
export class ProjectService {

	constructor(private http: Http) {
	}

	getProjects(): Observable<any> {
		return this.http.get('http://project-history.lc/api/project')
			.map(
				(response: Response) => {
					return response.json().projects;
				}
		);
	}

	createProject(name: string): Observable<any> {
		const body = JSON.stringify({name: name});
		const headers = new Headers({'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/project/create', body, {headers: headers})
			.map(
				(response: Response) => {
					return response.json().project;
				}
			);
	}

	updateProject(id: number, newName: string) {
		const body = JSON.stringify({name: newName});
		const headers = new Headers({'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/project/edit/' + id, body, {headers: headers})
			.map(
				(response: Response) => response.json()
			);
	}

	deleteProject(id: number) {
		return this.http.delete('http://project-history.lc/api/project/delete/' + id);
	}

}