import { Component, OnInit, Input } from '@angular/core';
import { Response } from "@angular/http";
import { NgForm } from '@angular/forms';

import { Project } from "../project.interface";
import { ProjectService } from "../project.service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  projects: Project[];
  project: Project;
  create = false;
  list = true;

  constructor(private projectService: ProjectService) { }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe(
        (projects: Project[]) => this.projects = projects,
        (error: Response) => console.log(error)
    );
  }

  ngOnInit(): void {
    this.getProjects();
  }

  onCreate() {
    this.create = true;
    this.list = false;
  }

  onCancel() {
    this.create = false;
    this.list = true;
  }

  onDelete(project: Project) {
    const position = this.projects.findIndex(
      (projectEl: Project) => {
        return projectEl.id == project.id;
      }
    );
    this.projects.splice(position, 1);
  }

  onSubmit(form: NgForm) {
    this.projectService.createProject(form.value.name)
      .subscribe(
        (project: Project) => this.project = project
      );
    console.log(this.project);
    form.reset();
    this.create = false;
    this.list = true;
    //this.projects.splice(0, 0, this.project);
  }

}
