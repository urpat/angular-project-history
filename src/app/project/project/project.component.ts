import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Project } from "../project.interface";
import { ProjectService } from "../project.service";
import { ProjectsComponent} from "../projects/projects.component";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  @Input() project: Project;
  @Output() projectDelete = new EventEmitter<Project>();
  edit = false;
  editValue = '';
  show = false;

  constructor(
    private projectService: ProjectService,
    private projects: ProjectsComponent,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  onCancel() {
  	this.edit = false;
  }

  onEdit() {
  	this.edit = true;
  	this.editValue = this.project.name;
  }

  onUpdate() {
  	this.projectService.updateProject(this.project.id, this.editValue)
  		.subscribe(
  			(project: Project) => {
  				this.project.name = this.editValue;
  				this.editValue = '';
  			}
  		);
  	this.edit = false;
  }

  onDelete() {
    this.projectService.deleteProject(this.project.id)
      .subscribe(
        () => { 
        	this.projectDelete.emit(this.project);
        	console.log('Project deleted');
        }
      );
  }

  onAddUser() {

  }

  onSelect(project: Project) {
    this.router.navigate(['/show-project', project.id], {relativeTo: router});
  }

}
