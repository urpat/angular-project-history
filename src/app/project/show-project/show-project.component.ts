import { Component, OnInit, Input } from '@angular/core';

import { Project } from "../project.interface";
 
@Component({
  selector: 'app-show-project',
  templateUrl: './show-project.component.html',
  styleUrls: ['./show-project.component.css']
})
export class ShowProjectComponent implements OnInit {
  //@Input() project: Project;
  list = true;
  create = false;
  
  constructor() {       
  }

  ngOnInit() {
  }

  meetingCreate() {
  	this.list = false;
  	this.create = true;
  }

}