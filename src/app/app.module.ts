import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ProjectsComponent } from './project/projects/projects.component';
import { ProjectComponent } from './project/project/project.component';

import { ProjectService } from "./project/project.service";

import { routing } from "./app.routing";
import { ShowProjectComponent } from './project/show-project/show-project.component';
import { MeetingComponent } from './meeting/meeting.component';
import { MeetingPlanComponent } from './meeting-plan/meeting-plan.component';
import { MeetingReportComponent } from './meeting-report/meeting-report.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    ProjectComponent,
    ShowProjectComponent,
    MeetingComponent,
    MeetingPlanComponent,
    MeetingReportComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [
    ProjectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
