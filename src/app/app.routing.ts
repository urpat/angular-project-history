import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ProjectsComponent } from "./project/projects/projects.component";
import { ShowProjectComponent } from "./project/show-project/show-project.component";

const APP_ROUTES: Routes = [
	{ path: '', component: ProjectsComponent },
	{ path: 'show-project/:id', component: ShowProjectComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);